﻿using System;
using Persistencia.Persistencias;

namespace Executavel
{
    class Program
    {
        static void Main(string[] args)
        {
            PersistenciaPessoa persistencia = new PersistenciaPessoa();
            if (persistencia.Deletar(2))
                Console.Write("OK");
            else
                Console.Write("NAO");

            Console.ReadKey();
        }
    }
}
