﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Models
{
    public class Pessoa
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Login { get; set; }
        public string Senha { get; set; }

        public Pessoa(string nome, string login, string senha)
        {
            Nome = nome;
            Login = login;
            Senha = senha;
        }

        public Pessoa() : this(null, null, null) { }

        public override string ToString() => Id + ", " + Nome + ", " + Login + ", " + Senha;
    }
}
