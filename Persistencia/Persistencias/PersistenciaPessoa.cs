﻿using System;
using System.Collections.Generic;
using System.Text;
using Model.Models;
using MySql.Data.MySqlClient;

namespace Persistencia.Persistencias
{
    public class PersistenciaPessoa : IDaoGenerico<Pessoa>
    {
        private static MySqlConnection conexao;

        public PersistenciaPessoa() { if (conexao == null) conexao = Conexao.AbrirConexao(); }

        public bool Adicionar(Pessoa p)
        {
            try
            {
                // Query
                string sql = "INSERT INTO pessoa (nome, login, senha) VALUES (@nome, @login, @senha)";

                // Objeto para executar o SQL e setando a conexão.
                using (MySqlCommand comando = new MySqlCommand(sql, conexao))
                {
                    // Preparando os valores.
                    comando.Parameters.AddWithValue("@nome", p.Nome);
                    comando.Parameters.AddWithValue("@login", p.Login);
                    comando.Parameters.AddWithValue("@senha", p.Senha);

                    comando.Prepare();

                    // Executando a query
                    comando.ExecuteNonQuery();
                }
            }
            catch (MySqlException e)
            {
                Console.WriteLine("Erro ao inserir a pessoa, codigo: " + e.ToString());
                return false;
            }
            return true;
        }

        public bool Atualizar(Pessoa p)
        {
            try
            {
                // Query SQL
                string sql = "UPDATE pessoa SET nome = @nome, login = @login, senha = @senha WHERE id = @id";

                // Abrindo Conexao e iniciando o manipulador de SQL.
                using (MySqlCommand sqlCommand = new MySqlCommand(sql, conexao))
                {
                    // Preparando os itens
                    sqlCommand.Parameters.AddWithValue("@id", p.Id);
                    sqlCommand.Parameters.AddWithValue("@nome", p.Nome);
                    sqlCommand.Parameters.AddWithValue("@login", p.Login);
                    sqlCommand.Parameters.AddWithValue("@senha", p.Senha);

                    sqlCommand.Prepare();

                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (MySqlException e)
            {
                Console.WriteLine("Erro Atualizar, codigo:" + e.ToString());
                return false;
            }
            return true;
        }

        public bool Deletar(int id)
        {
            try
            {
                // String
                string sql = "DELETE FROM pessoa WHERE id = @id";

                // Apenas outro modo de fazer, auxilia na melhor organização do código.
                using (MySqlCommand comando = new MySqlCommand(sql, conexao))
                {
                    comando.Parameters.AddWithValue("@id", id);
                    comando.Prepare();

                    if (comando.ExecuteNonQuery() == 0)
                        return false;
                }
            }
            catch (MySqlException e)
            {
                Console.WriteLine("Erro ao remover, codigo: " + e.ToString());
                return false;
            }
            return true;
        }

        public Pessoa ObterPorId(int id)
        {
            Pessoa p;
            try
            {
                string sql = "SELECT * FROM pessoa WHERE id = @id";

                using (MySqlCommand cmd = new MySqlCommand(sql, conexao))
                {
                    cmd.Parameters.AddWithValue("@id", id);

                    MySqlDataReader leitor = cmd.ExecuteReader();

                    if (leitor.Read())
                        //if (leitor.HasRows)
                        // Apenas um novo metodo de instanciar o objeto.
                        p = new Pessoa()
                        {
                            Id = (int)leitor.GetValue(0),
                            Nome = (string)leitor.GetValue(1),
                            Login = (string)leitor.GetValue(2),
                            Senha = (string)leitor.GetValue(3)
                        };
                    else
                        return null; // Retorna null caso não exista registro com o ID.
                }
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
            return p;
        }

        public List<Pessoa> ObterTodos()
        {
            List<Pessoa> pessoas;
            try
            {
                string sql = "SELECT * FROM pessoa";

                using (MySqlCommand cmd = new MySqlCommand(sql, conexao))
                {
                    MySqlDataReader leitor = cmd.ExecuteReader();

                    pessoas = new List<Pessoa>();

                    if (leitor.HasRows)
                        while (leitor.Read())
                        {
                            pessoas.Add(new Pessoa()
                            {
                                Id = (int)leitor.GetValue(0),
                                Nome = (string)leitor.GetValue(1),
                                Login = (string)leitor.GetValue(2),
                                Senha = (string)leitor.GetValue(3)
                            });
                        }
                    else
                        return null;
                }
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
            return pessoas;
        }
    }
}