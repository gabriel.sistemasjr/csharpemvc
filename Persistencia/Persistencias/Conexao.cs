﻿using System;
using System.Collections.Generic;
using System.Text;
using MySql.Data.MySqlClient;

namespace Persistencia.Persistencias
{
    public class Conexao
    {
        private static MySqlConnection con; // Variavel de Conexao

        // Abrir a Conexao
        public static MySqlConnection AbrirConexao()
        {
            //  Testando se a conexao já existe, caso exista, retorna direto.
            if (con == null)
                try
                {
                    // Variaveis de Conexao.
                    string stringConexao = "server=localhost;database=db_csharp;uid=root;";
                    con = new MySqlConnection(stringConexao); // variavel de conexao

                    con.Open(); // Abrindo Conexao
                }
                catch (Exception e)
                {
                    Console.WriteLine("Conexão falhou, erro: " + e.ToString());
                }

            // Retornando a conexao.
            return con;
        }
    }
}