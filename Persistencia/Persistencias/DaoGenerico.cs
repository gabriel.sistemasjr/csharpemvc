﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Persistencia.Persistencias
{
    interface IDaoGenerico<T> where T : class
    {
        bool Adicionar(T objeto);
        bool Atualizar(T objeto);
        bool Deletar(int id);
        T ObterPorId(int id);
        List<T> ObterTodos();
    }
}
